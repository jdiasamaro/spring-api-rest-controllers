# Dynamic @RequestMapping paths using Spring Boot (2.0.0) and some reflection

When running a web application, usually we need to expose not only frontend resources that will render the application in a browser but also some APIs that go along the application (ie. login endpoints, CRUD endpoints, etc.).

This simple project is an example of how we can expose some resources with a given base path and other resources, REST controllers for instance, with another path (ie. `/api`). All of this programatically and in a dynamic way.

Spring Boot offers tools that help us to expose resources or endpoints:

1. `@Controller` - serve web application views typically using, but not restricted to, Spring MVC or even static resources
2. `@RestController` - expose REST APIs that might be consumed by the frontend 

If we don't change anything, the controllers will be served on the default root URL path `/`. However, there are several features on Spring that can change this behavior. The following list does not pretent to be an extensive list but here are 3 of the most simple tricks:

## 1. Using the property `server.servlet.contextPath`

The most simple option we have is to use this property. When configured on the properties file, the context path will be applied to the whole application including:

* static resources
* `@Controller`
* `@RestController`
* `@BasePathAwareContoller`
* `@RepositoryRestController`
* `@RepositoryRestResource`

Simple but also limited. All the application will be affected by this.


## 2. Importing the Spring Data REST module

By importing this Spring project, you can access the annotation @BasePathAwareController. As the name implies, this makes your controller aware of the property `spring.data.rest.base-path` and they will prepend whatever path you define in the property to their own request mapping path. The full list of the bean types that this annotation applies to are:

* `@BasePathAwareContoller`
* `@RepositoryRestController`
* `@RepositoryRestResource`

Although more dynamic compared to our 1st option, the downsides of this solution are: 
 
1. By importing this module, Spring Boot will inherently expose all the CRUD endpoints for Spring Data REST repositories, which might not be what we are looking for;
2. Also this is a single property. Either we use it or not. All our endpoints marked with those annotations will prepend their paths with the configurated value.


## 3. Dynamically change the `@RequestMapping` annotation before Spring Boot loads the endpoints

This is the solution that this small projects aims at. As an example, this project will enhance the REST controllers with a `/api` prefix. The steps of this solution include 2 simple steps:

1. Creating a custom annotation called `@ApiRestController`

    ```java
    @Documented
    @RestController
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ApiRestController {
        
    }
    ```

2. Creating a "substitute" @RequestMapping that will hold the `/api` prefix and also the already defined path, if any

    This substitue can be defined to make the prefix even more dynamic, by reading it from a properties file or by selecting which mappings to change. This example adjusts both `value()` and `path()` parameters and prepends the new prefix.

3. Iterating over all desired controllers before Spring context loading and changing their `@RequestMapping` values at runtime

    ```java
    @Controller
    @SpringBootApplication
    public class DynamicRequestMappingPathsApplication {
    
    	public static void main(String[] args) {
            enhanceApiRestControllers(); // <<< MAGIC HAPPENS HERE
    		SpringApplication.run(DynamicRequestMappingPathsApplication.class, args);
    	}
    }
    ```

# To sum it up

It is possible to create a more dynamic mapping for our REST controllers so that they reside in a different base path of all the othe resources. This can also be as dynamic as necessary. This project uses [this really nice Reflections library](https://github.com/ronmamo/reflections), but even this can be simplified if all the controllers are known before hand or if we want to iterate over the classpath (which is implementation-dependent and might not yield the necessary results). The main part of the project is:

```java
public class ApiRestControllerEnhancer {

    public static void enhanceApiRestControllers() {
        new Reflections("jdiasamaro.spring.example.controllers")
                .getTypesAnnotatedWith(ApiRestController.class)
                .forEach(ApiRestControllerEnhancer::enhanceApiRestController);
    }

    @SuppressWarnings("unchecked")
    private static void enhanceApiRestController(Class<?> classToChange) {
        if (!classToChange.isAnnotationPresent(RequestMapping.class)) {
            return;
        }

        try {
            RequestMapping newRequestMapping = new ApiRequestMapping(classToChange.getAnnotation(RequestMapping.class));
            Method method = Class.class.getDeclaredMethod("annotationData", null);
            method.setAccessible(true);

            Object annotationData = method.invoke(classToChange);
            Field annotations = annotationData.getClass().getDeclaredField("annotations");
            annotations.setAccessible(true);

            Map<Class<? extends Annotation>, Annotation> map = (Map<Class<? extends Annotation>, Annotation>) annotations.get(annotationData);
            map.put(RequestMapping.class, newRequestMapping);
        } catch (Exception e) {
            throw new IllegalStateException("It was not possible to enhance the REST API controllers", e);
        }
    }
}
```