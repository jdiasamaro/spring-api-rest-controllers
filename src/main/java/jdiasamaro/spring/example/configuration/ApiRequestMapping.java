package jdiasamaro.spring.example.configuration;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.Annotation;

import static java.util.Arrays.stream;

public class ApiRequestMapping implements RequestMapping {

    private String name;
    private String[] value;
    private String[] path;
    private RequestMethod[] method;
    private String[] params;
    private String[] headers;
    private String[] consumes;
    private String[] produces;

    public ApiRequestMapping(RequestMapping requestMapping) {
        this.value = enhancePaths(requestMapping.value());
        this.path = enhancePaths(requestMapping.path());
        this.name = requestMapping.name();
        this.method = requestMapping.method();
        this.params = requestMapping.params();
        this.headers = requestMapping.headers();
        this.consumes = requestMapping.consumes();
        this.produces = requestMapping.produces();
    }

    private static String[] enhancePaths(String[] paths) {
        return stream(paths)
                .filter(path -> path != null && !path.isEmpty())
                .map(path -> "/api/" + (path.startsWith("/") ? path.substring(1, path.length()) : path))
                .toArray(String[]::new);
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String[] value() {
        return this.value;
    }

    @Override
    public String[] path() {
        return this.path;
    }

    @Override
    public RequestMethod[] method() {
        return this.method;
    }

    @Override
    public String[] params() {
        return this.params;
    }

    @Override
    public String[] headers() {
        return this.headers;
    }

    @Override
    public String[] consumes() {
        return this.consumes;
    }

    @Override
    public String[] produces() {
        return this.produces;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return RequestMapping.class;
    }
}
