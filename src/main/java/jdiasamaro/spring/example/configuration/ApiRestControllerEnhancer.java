package jdiasamaro.spring.example.configuration;

import org.reflections.Reflections;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

public class ApiRestControllerEnhancer {

    public static void enhanceApiRestControllers() {
        new Reflections("jdiasamaro.spring.example.controllers")
                .getTypesAnnotatedWith(ApiRestController.class)
                .forEach(ApiRestControllerEnhancer::enhanceApiRestController);
    }

    @SuppressWarnings("unchecked")
    private static void enhanceApiRestController(Class<?> classToChange) {
        if (!classToChange.isAnnotationPresent(RequestMapping.class)) {
            return;
        }

        try {
            RequestMapping newRequestMapping = new ApiRequestMapping(classToChange.getAnnotation(RequestMapping.class));
            Method method = Class.class.getDeclaredMethod("annotationData", null);
            method.setAccessible(true);

            Object annotationData = method.invoke(classToChange);
            Field annotations = annotationData.getClass().getDeclaredField("annotations");
            annotations.setAccessible(true);

            Map<Class<? extends Annotation>, Annotation> map = (Map<Class<? extends Annotation>, Annotation>) annotations.get(annotationData);
            map.put(RequestMapping.class, newRequestMapping);
        } catch (Exception e) {
            throw new IllegalStateException("It was not possible to enhance the REST API controllers", e);
        }
    }
}
