package jdiasamaro.spring.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static jdiasamaro.spring.example.configuration.ApiRestControllerEnhancer.enhanceApiRestControllers;

@Controller
@SpringBootApplication
public class DynamicRequestMappingPathsApplication {

	public static void main(String[] args) {
        enhanceApiRestControllers();
		SpringApplication.run(DynamicRequestMappingPathsApplication.class, args);
	}

	@RequestMapping("/")
	public String index() {
	    return "index.html";
    }
}
