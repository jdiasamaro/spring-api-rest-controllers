package jdiasamaro.spring.example.controllers;

import jdiasamaro.spring.example.configuration.ApiRestController;
import jdiasamaro.spring.example.services.api.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.ResponseEntity.*;

@ApiRestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping
    public ResponseEntity login(@RequestParam String credentials) {
        try {
            String sessionToken = authenticationService.login(credentials);
            return ok(sessionToken);
        } catch (Exception e) {
            return status(UNAUTHORIZED).build();
        }
    }

    @DeleteMapping
    public ResponseEntity logout(@RequestHeader(AUTHORIZATION) String authorizationHeader) {
        try {
            authenticationService.logout(authorizationHeader.replaceFirst("Bearer ", ""));
            return noContent().build();
        } catch (Exception e) {
            return badRequest().build();
        }
    }
}
