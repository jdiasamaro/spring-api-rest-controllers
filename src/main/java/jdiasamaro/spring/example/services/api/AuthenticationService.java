package jdiasamaro.spring.example.services.api;

public interface AuthenticationService {

    /**
     * Starts a user session based on its user:pass credentials
     *
     * @param base64Credentials in Base 64 with the format user:password
     * @return a session token
     */
    String login(String base64Credentials);

    /**
     * Revokes a user session token
     *
     * @param sessionToken the session token to revoke
     */
    void logout(String sessionToken);
}
