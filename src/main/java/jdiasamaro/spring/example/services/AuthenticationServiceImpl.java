package jdiasamaro.spring.example.services;

import jdiasamaro.spring.example.services.api.AuthenticationService;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.UUID;

@Service
public class AuthenticationServiceImpl implements AuthenticationService  {

    @Override
    public String login(String base64Credentials) {
        String userPass = new String(Base64.getDecoder().decode(base64Credentials));
        String[] credentials = userPass.split(":");

        if (credentials.length == 2) {
            return UUID.randomUUID().toString();
        } else {
            throw new IllegalArgumentException("Invalid user/pass credentials");
        }
    }

    @Override
    public void logout(String sessionToken) {
        if (sessionToken == null || sessionToken.trim().isEmpty()) {
            throw new IllegalArgumentException("Invalid session token");
        }
    }
}
